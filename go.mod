module gitlab.com/shackra/goimapnotify

go 1.21

require (
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-imap-id v0.0.0-20190926060100-f94a56b9ecde
	github.com/emersion/go-imap-idle v0.0.0-20210907174914-db2568431445
	github.com/emersion/go-sasl v0.0.0-20231106173351-e73c9f7bad43
	github.com/fatih/color v1.16.0
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
